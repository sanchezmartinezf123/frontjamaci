import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ContactosComponent } from './contactos/contactos.component';
import { ProductosComponent } from './productos/productos.component';
import { CitasComponent } from './citas/citas.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AdmiRoutingModule } from '../admi/admi-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSliderModule } from '@angular/material/slider';



@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    NosotrosComponent,
    ServiciosComponent,
    ContactosComponent,
    ProductosComponent,
    CitasComponent,
    
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatSlideToggleModule,
    
CommonModule,
AdmiRoutingModule,
MatFormFieldModule,
MatInputModule,
MatButtonModule,
ReactiveFormsModule,
MatSnackBarModule,
MatProgressSpinnerModule,
MatToolbarModule,
MatIconModule,
MatTableModule,
MatCardModule,
MatGridListModule,
MatSelectModule,
MatTooltipModule,
MatPaginatorModule,
MatSortModule,
MatDialogModule,
MatListModule,
MatIconModule,
MatDividerModule,
MatSidenavModule,
MatMenuModule,
MatDatepickerModule
  ],
  
exports:[
  MatSliderModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  ReactiveFormsModule,
  MatSnackBarModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatCardModule,
  MatGridListModule,
  MatSelectModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatSortModule,
  MatDialogModule,
  MatListModule,
  MatIconModule,
  MatDividerModule,
  MatSidenavModule,
  MatMenuModule,
  MatDatepickerModule
  ]
})
export class DashboardModule { }


