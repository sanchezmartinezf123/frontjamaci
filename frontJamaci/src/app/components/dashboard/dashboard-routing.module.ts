import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ContactosComponent } from './contactos/contactos.component';
import { LoginComponent } from './login/login.component';
import { ProductosComponent } from './productos/productos.component';
import { CitasComponent } from './citas/citas.component';
import { RecuContraComponent } from './recu-contra/recu-contra.component';
import { RecuCodigoComponent } from './recu-codigo/recu-codigo.component';
import { RecuRestablecerComponent } from './recu-restablecer/recu-restablecer.component';

const routes: Routes = [
  {
    path:'',
    children: [
    {path:'home',component:HomeComponent},
    {path:'nosotros',component:NosotrosComponent},
    {path:'servicios',component:ServiciosComponent},
    {path:'productos',component:ProductosComponent},
    {path:'contactos',component:ContactosComponent},
    {path:'citas',component:CitasComponent},
    {path:'login',component:LoginComponent},
    {path:'recuContra',component:RecuContraComponent},
    {path:'recuCodigo',component:RecuCodigoComponent},
    {path:'recuRestablecer',component:RecuRestablecerComponent}

  ]}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
