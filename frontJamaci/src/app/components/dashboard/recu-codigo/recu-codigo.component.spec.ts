import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuCodigoComponent } from './recu-codigo.component';

describe('RecuCodigoComponent', () => {
  let component: RecuCodigoComponent;
  let fixture: ComponentFixture<RecuCodigoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecuCodigoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecuCodigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
