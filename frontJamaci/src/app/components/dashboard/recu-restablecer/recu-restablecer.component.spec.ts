import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecuRestablecerComponent } from './recu-restablecer.component';

describe('RecuRestablecerComponent', () => {
  let component: RecuRestablecerComponent;
  let fixture: ComponentFixture<RecuRestablecerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecuRestablecerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RecuRestablecerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
