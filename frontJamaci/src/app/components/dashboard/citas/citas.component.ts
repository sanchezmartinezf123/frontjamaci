import { Component } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
let fecha:string;

interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-citas',
  templateUrl: './citas.component.html',
  styleUrls: ['./citas.component.css']
})
export class CitasComponent {
  events: string[] = [];
  
  addEvent(event: MatDatepickerInputEvent<Date>) {
    fecha=`${event.value?.getDate()}/${event.value?.getMonth()}/${event.value?.getFullYear()}`;
    console.log(fecha);
    if (fecha==='18/0/2023') {
      console.log('Fecha disponible')
    }else{
      console.log('Fecha no disponible')
    }
    

  }

  foods: Food[] = [
    {value: 'steak-0', viewValue: '11:00'},
    {value: 'pizza-1', viewValue: '12:00'},
    {value: 'tacos-2', viewValue: '13:00'},
  ];
  
}
