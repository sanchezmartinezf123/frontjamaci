import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitasComponent } from './citas/citas.component';
import { AdminLecturaComponent } from './admin-lectura.component';
import { AdminLecturaRoutingModule } from './admin-lectura-routing.module';
import { AgendarComponent } from './agendar/agendar.component';



@NgModule({
  declarations: [
    CitasComponent,
    AdminLecturaComponent,
    AgendarComponent
  ],
  imports: [
    CommonModule,
    AdminLecturaRoutingModule
  ]
})
export class AdminLecturaModule { }
