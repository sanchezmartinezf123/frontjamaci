import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLecturaComponent } from './admin-lectura.component';

describe('AdminLecturaComponent', () => {
  let component: AdminLecturaComponent;
  let fixture: ComponentFixture<AdminLecturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminLecturaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminLecturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
