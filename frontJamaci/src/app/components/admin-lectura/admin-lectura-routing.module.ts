import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CitasComponent } from './citas/citas.component';
import { AgendarComponent } from './agendar/agendar.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path:'citas', component:CitasComponent },
      { path:'agendar', component:AgendarComponent }
      
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminLecturaRoutingModule { }
