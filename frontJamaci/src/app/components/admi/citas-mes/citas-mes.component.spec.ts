import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CitasMesComponent } from './citas-mes.component';

describe('CitasMesComponent', () => {
  let component: CitasMesComponent;
  let fixture: ComponentFixture<CitasMesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CitasMesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CitasMesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
