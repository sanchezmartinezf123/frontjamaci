import { Component } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

let fecha:string;

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-citas-reagendar',
  templateUrl: './citas-reagendar.component.html',
  styleUrls: ['./citas-reagendar.component.css']
})


export class CitasReagendarComponent {
  events: string[] = [];
  
  addEvent(event: MatDatepickerInputEvent<Date>) {
    fecha=`${event.value?.getDate()}/${event.value?.getMonth()}/${event.value?.getFullYear()}`;
    console.log(fecha);
    if (fecha==='18/0/2023') {
      console.log('Fecha disponible')
    }else{
      console.log('Fecha no disponible')
    }
    

  }

  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'},
  ];
  
}
