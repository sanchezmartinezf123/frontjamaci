import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CitasReagendarComponent } from './citas-reagendar.component';

describe('CitasReagendarComponent', () => {
  let component: CitasReagendarComponent;
  let fixture: ComponentFixture<CitasReagendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CitasReagendarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CitasReagendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
