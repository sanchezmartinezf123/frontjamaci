import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CitasComponent } from './citas/citas.component';
import { CitasMesComponent } from './citas-mes/citas-mes.component';
import { CitasPendientesComponent } from './citas-pendientes/citas-pendientes.component';
import { CitasReagendarComponent } from './citas-reagendar/citas-reagendar.component';
import { MatNativeDateModule } from '@angular/material/core';

const routes: Routes = [
  {
    path: '',
    children: [
      { path:'citas', component:CitasComponent },
      { path: 'citas-mes', component:CitasMesComponent },
      { path: 'citas-pendientes', component:CitasPendientesComponent },
      { path: 'citas-reagendar', component:CitasReagendarComponent },
    ]
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  exports: [RouterModule,MatNativeDateModule]
})
export class AdmiRoutingModule { }
