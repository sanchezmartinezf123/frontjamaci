import { Component } from '@angular/core';
interface itemMenu {
  title: string,
  url: string
}
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  menuT:itemMenu[] = [{
    title: 'Home',
    url: '/dashboard/home'
  },
  {
    title: 'Nosotros',
    url: '/dashboard/nosotros'
  },
  {
    title: 'Servicios',
    url: '/dashboard/servicios'
    },
  {
      title: 'Productos',
      url: '/dashboard/productos' 
    },
  {
    title: 'Contactos',
    url: '/dashboard/contactos'
  },
  {
    title: 'Login/Registrate',
    url: '/dashboard/login'
  }

]

}

