import { Component } from '@angular/core';

interface itemMenu {
  title: string,
  url: string
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {

  menuT:itemMenu[] = [{
    title: 'Home',
    url: '/dashboard/home'
  },
  {
    title: 'Nosotros',
    url: '/dashboard/nosotros'
  },
  {
    title: 'Servicios',
    url: '/dashboard/servicios'
    },
    {
      title: 'Productos',
      url: '/dashboard/productos'
    },
  {
    title: 'Contactos',
    url: '/dashboard/contactos'
  },
  {
    title: 'Login/Registrate',
    url: '/dashboard/login'
  }

]

}
