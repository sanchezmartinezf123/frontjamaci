import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes =[
  {
    path: 'dashboard',
    loadChildren: () => import('./components/dashboard/dashboard.module').then(m=>m.DashboardModule)
  },
  {
    path: 'admi',
    loadChildren: () => import('./components/admi/admi.module').then(m=>m.AdmiModule)
 },
  {
    path: 'admin-lectura',
    loadChildren: () => import('./components/admin-lectura/admin-lectura.module').then(m=>m.AdminLecturaModule)
  } 
 
]

@NgModule({
  declarations: [],
  imports: [CommonModule,RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
